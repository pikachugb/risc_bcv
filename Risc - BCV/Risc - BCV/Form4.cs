﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Globalization;

namespace Risc___BCV
{
    
    public partial class Form4 : Form
    {
        

        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Puiu Moldoveanu\documents\visual studio 2010\Projects\Risc - BCV\Risc - BCV\Riscuri.mdf;Integrated Security=True;User Instance=True");
        SqlDataReader dr;
        SqlCommand cmd1 = new SqlCommand();
        SqlConnection cn1 = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Puiu Moldoveanu\documents\visual studio 2010\Projects\Risc - BCV\Risc - BCV\Riscuri.mdf;Integrated Security=True;User Instance=True");
        
        SqlCommand cmd2 = new SqlCommand();
        SqlConnection cn2 = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Puiu Moldoveanu\documents\visual studio 2010\Projects\Risc - BCV\Risc - BCV\Riscuri.mdf;Integrated Security=True;User Instance=True");
        SqlDataReader dr2;
        public int IDA;
        public Form4()
        {
            InitializeComponent();
            cmd.Connection = cn;
            cmd1.Connection = cn1;
            cmd2.Connection = cn2;
            
        }

        private void iesireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            textBox1.ReadOnly = true;
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            textBox5.ReadOnly = true;
            textBox6.ReadOnly = true;
            groupBox1.Visible = false;
            groupBox3.Visible = false;
            label16.Visible = false;
            comboBox2.Visible = false;
            string ex = "";
            cn.Open();
            cmd.CommandText = "Select Nume from DatePersonale";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (dr[0].ToString() != ex)
                    {
                        comboBox1.Items.Add(dr[0].ToString());
                    }
                    ex = dr[0].ToString();
                }
            }
            cn.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label16.Visible = true;
            comboBox2.Visible = true;
            comboBox2.Items.Clear();
            cn.Open();
            cmd.CommandText = "Select Prenume from DatePersonale where Nume = '" + comboBox1.SelectedItem.ToString() + "' ";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    comboBox2.Items.Add(dr[0].ToString());
                }
            }
            cn.Close();
        }

        public static string TruncateLongString(string str, int maxLength)
        {
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }
    
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox3.Visible = true;
            label17.Visible = true;
            comboBox3.Items.Clear();
            cn.Open();
            cmd.CommandText = "Select Data_Analiza from AnalizePacienti where ID_Pacient = (Select ID_Pacient from DatePersonale where Nume = '"+comboBox1.SelectedItem.ToString()+"' and Prenume = '"+comboBox2.SelectedItem.ToString()+"' ) ";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    comboBox3.Items.Add(TruncateLongString(dr[0].ToString(), 10));
                }
            }
            else
            {
                groupBox1.Visible = true;
                cn2.Open();
                cmd2.CommandText = "Select ID_Pacient,Nume,Prenume,Gen,Varsta,Data_Nasterii,Email from DatePersonale where Nume = '" + comboBox1.SelectedItem.ToString() + "' and Prenume = '" + comboBox2.SelectedItem.ToString() + "' ";
                dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    textBox1.Text = dr2[0].ToString();
                    textBox2.Text = dr2[1].ToString();
                    textBox3.Text = dr2[2].ToString();
                    if (dr2[3].ToString() == "M")
                    {
                        radioButton1.Checked = true;
                    }
                    else
                    {
                        radioButton2.Checked = true;
                    }
                    textBox4.Text = dr2[4].ToString();
                    textBox5.Text = TruncateLongString(dr2[5].ToString(), 10);
                    textBox6.Text = dr2[6].ToString();
                }
                cn2.Close();
                groupBox3.Visible = true;
                button1.Visible = false;
                button2.Visible = false;                
                button4.Visible = false;                    
                textBox7.ReadOnly = false;                
                textBox8.ReadOnly = false;                
                textBox9.ReadOnly = false;
                textBox10.ReadOnly = false;                
                textBox11.Visible = false;                
                dateTimePicker1.Visible = true;                
                checkBox1.Enabled = true;                
                checkBox2.Enabled = true;
            }
            cn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox7.ReadOnly = false;
            textBox8.ReadOnly = false;
            textBox9.ReadOnly = false;
            textBox10.ReadOnly = false;
            textBox11.Visible = false;
            dateTimePicker1.Visible = true;
            checkBox1.Enabled = true;
            checkBox2.Enabled = true;
            dateTimePicker1.Value = Convert.ToDateTime(textBox11.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cn.Open();
            cmd.CommandText = "Update AnalizePacienti set Data_Analiza = '" + dateTimePicker1.Value.ToString("yyyy-MM-dd") + "' , Colesterol_Total = '"+Convert.ToDouble(textBox7.Text)+"' , HDL = '"+Convert.ToDouble(textBox8.Text)+"', TAS = '"+Convert.ToDouble(textBox9.Text)+"', PCR = '"+textBox10.Text+"', Fumator = '"+checkBox1.Checked+"', BCVF = '"+checkBox2.Checked+"' where ID_Pacient = '"+Convert.ToDouble(textBox1.Text)+"' ";
            cmd.ExecuteNonQuery();
            cn.Close();
            MessageBox.Show("Analize modificate");
            textBox1.ReadOnly = true;
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            textBox5.ReadOnly = true;
            textBox6.ReadOnly = true;
            checkBox1.Enabled = false;
            checkBox2.Enabled = false;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            if (button3.Text == "Adauga analiza")
            {
                button1.Visible = false;
                button2.Visible = false;
                button4.Visible = false;
                textBox7.ReadOnly = false;
                textBox8.ReadOnly = false;
                textBox9.ReadOnly = false;
                textBox10.ReadOnly = false;
                textBox11.Visible = false;
                dateTimePicker1.Visible = true;
                checkBox1.Enabled = true;
                checkBox2.Enabled = true;
                textBox7.Text = "";
                textBox8.Text = "";
                textBox9.Text = "";
                textBox10.Text = "";
                textBox11.Text = "";
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                button3.Text = "Adauga";
            }
            else
            {
                cn2.Open();
                cmd2.CommandText = "Insert into AnalizePacienti(ID_Pacient,Data_Analiza, Colesterol_Total,HDL,TAS,PCR,Fumator,BCVF) values ('" + Convert.ToDouble(textBox1.Text) + "' ,'" + dateTimePicker1.Value.ToString("yyyy-MM-dd") +"','" + Convert.ToDouble(textBox7.Text) + "','" + Convert.ToDouble(textBox8.Text) + "','" + Convert.ToDouble(textBox9.Text) + "', '" + textBox10.Text + "' , '" + checkBox1.Checked + "','" + checkBox2.Checked + "') ";
                cmd2.ExecuteNonQuery();
                cn2.Close();
                MessageBox.Show("Datele pentru analize au fost adaugate!");
                button1.Visible = true;
                button2.Visible = true;
                button4.Visible = true;
                textBox7.ReadOnly = true;
                textBox8.ReadOnly = true;
                textBox9.ReadOnly = true;
                textBox10.ReadOnly = true;
                textBox11.Visible = true;
                dateTimePicker1.Visible = false;
                checkBox1.Enabled = false;
                checkBox2.Enabled = false;
                textBox11.Text = TruncateLongString(dateTimePicker1.Value.Date.ToString(), 10);
                button3.Text = "Adauga analiza";
                comboBox3.Items.Clear();
                cn.Open();
                cmd.CommandText = "Select Data_Analiza from AnalizePacienti where ID_Pacient = (Select ID_Pacient from DatePersonale where Nume = '" + comboBox1.SelectedItem.ToString() + "' and Prenume = '" + comboBox2.SelectedItem.ToString() + "' ) ";
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        comboBox3.Items.Add(TruncateLongString(dr[0].ToString(), 10));
                    }
                }
                cn.Close();

               }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            groupBox1.Visible = true;
            cn.Open();
            cmd.CommandText = "Select ID_Pacient,Nume,Prenume,Gen,Varsta,Data_Nasterii,Email from DatePersonale where Nume = '" + comboBox1.SelectedItem.ToString() + "' and Prenume = '" + comboBox2.SelectedItem.ToString() + "' ";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                textBox1.Text = dr[0].ToString();
                textBox2.Text = dr[1].ToString();
                textBox3.Text = dr[2].ToString();
                if (dr[3].ToString() == "M")
                {
                    radioButton1.Checked = true;
                }
                else
                {
                    radioButton2.Checked = true;
                }
                textBox4.Text = dr[4].ToString();
                textBox5.Text = TruncateLongString(dr[5].ToString(), 10);
                textBox6.Text = dr[6].ToString();
            }
            cn.Close();                           
        }

        public void txt()
        {
            textBox11.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            checkBox1.Checked = false;
            checkBox2.Checked = false;
        }

        private void comboBox3_SelectedValueChanged(object sender, EventArgs e)
        {
            groupBox3.Visible = false;
            txt();
            groupBox3.Visible = true;
            cn2.Open();
            cmd2.CommandText = "Select Colesterol_Total,HDL,TAS,PCR,Fumator,BCVF,Data_Analiza,ID_Analiza from AnalizePacienti";
            dr2 = cmd2.ExecuteReader();
            string x;
            x = comboBox3.SelectedItem.ToString();
            if (dr2.HasRows)
            {
                while (dr2.Read())
                {     
                    if (TruncateLongString(dr2[6].ToString(), 10) == x)
                    {                        
                            IDA = int.Parse(dr2[7].ToString());
                            button1.Visible = true;
                            button2.Visible = true;
                            button3.Visible = true;
                            button4.Visible = true;
                            button3.Visible = true;
                            button3.Text = "Adauga analiza";
                            
                            textBox7.Text = dr2[0].ToString();
                            textBox8.Text = dr2[1].ToString();
                            textBox9.Text = dr2[2].ToString();
                            textBox10.Text = dr2[3].ToString();
                            if (dr2[4].ToString() == "true")
                            {
                                checkBox1.Checked = true;
                            }
                            else
                            {
                                checkBox1.Checked = false;
                            }
                            if (dr2[5].ToString() == "true")
                            {
                                checkBox2.Checked = true;
                            }
                            else
                            {
                                checkBox2.Checked = false;
                            }
                            textBox11.Text = TruncateLongString(dr2[6].ToString(), 10);
                    }
                }
            }
            cn2.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            cn.Open();
            cmd.CommandText = " DELETE FROM AnalizePacienti where ID_Analiza = '" + IDA + "'";
            cmd.ExecuteNonQuery();
            cn.Close();
            MessageBox.Show("Analiza stearsa");
            this.Close();
        }
    }
}
