﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Risc___BCV
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }


        private void panel1_Click(object sender, EventArgs e)
        {
            using (Graphics g = this.panel1.CreateGraphics())
            {
                Pen pen = new Pen(Color.Red, 2);
                Brush brush = new SolidBrush(this.panel1.BackColor);
                g.DrawRectangle(pen,100,100,100,100);
                pen.Dispose();
            }
        }
    }
}
