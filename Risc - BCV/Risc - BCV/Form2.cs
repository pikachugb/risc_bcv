﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Risc___BCV
{
    public partial class Form2 : Form
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=c:\users\puiu moldoveanu\documents\visual studio 2010\Projects\Risc - BCV\Risc - BCV\Riscuri.mdf;Integrated Security=True;User Instance=True");
        
        public Form2()
        {
            InitializeComponent();
            cmd.Connection = cn;
        }

        private void iesireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void sform3()
        {
            Application.Run(new Form3());
        }

        public void sform4()
        {
            Application.Run(new Form4());
        }

        private void adaugarePacientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(sform3));
            t.Start();
        }

        private void gestionareFisePacientiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(sform4));
            t.Start();
        }

        public void sform5()
        {
            Application.Run(new Form5());

        }

        private void graficInterpretareAnalizePacientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(sform5));
            t.Start();
        }
    }
}
