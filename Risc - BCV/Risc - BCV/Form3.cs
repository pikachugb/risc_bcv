﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Risc___BCV
{
    public partial class Form3 : Form
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=c:\users\puiu moldoveanu\documents\visual studio 2010\Projects\Risc - BCV\Risc - BCV\Riscuri.mdf;Integrated Security=True;User Instance=True");
        public Form3()
        {
            InitializeComponent();
            cmd.Connection = cn;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            textBox2.Select();
        }

        public bool verifemail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            dateTimePicker1.CalendarTitleBackColor = Color.White;
            DateTime today = DateTime.Today;
            int age = today.Year - dateTimePicker1.Value.Year;
            if (dateTimePicker1.Value > today.AddYears(-age))
            {
                age--;
            }
            textBox5.Clear();
            textBox5.Text = age.ToString();
        }

        public string radio()
        {
            if (radioButton1.Checked == true)
            {
                return radioButton1.Text;
            }
            else
            {
                return radioButton2.Text;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
                DateTime today = DateTime.Today;
                if (textBox2.Text != null && textBox3 != null && (radioButton1.Checked == true || radioButton2.Checked == true) && dateTimePicker1.Value < today)
                {
                    if (!verifemail(textBox4.Text))
                    {
                        MessageBox.Show("Adresa de email nu este valida!");
                    }
                    else
                    {
                        System.DateTime myDate = dateTimePicker1.Value;
                        cn.Open();
                        cmd.CommandText = "Insert DatePersonale(Nume,Prenume,Gen,Varsta,Data_Nasterii,Email) values ('" + textBox2.Text + "','" + textBox3.Text + "','" + radio() + "','" + textBox5.Text + "' , '" + dateTimePicker1.Value.Date + "', '" + textBox4.Text + "') ";
                        cmd.ExecuteNonQuery();
                        cn.Close();
                        MessageBox.Show("Pacient adaugat!");
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Completeaza toate campurile!");
                    if (textBox2.Text == "")
                    {
                        textBox2.BackColor = Color.Red;
                    }
                    if (textBox3.Text == "")
                    {
                        textBox3.BackColor = Color.Red;
                    }
                    if (radioButton1.Checked == false && radioButton2.Checked == false)
                    {
                        radioButton1.BackColor = Color.Red;
                        radioButton2.BackColor = Color.Red;
                    }
                    if (dateTimePicker1.Value > today)
                    {
                        dateTimePicker1.CalendarTitleBackColor = Color.Red;
                    }
                }
            }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.BackColor = Color.White;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            textBox3.BackColor = Color.White ;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            radioButton1.BackColor = DefaultBackColor;
            radioButton2.BackColor = DefaultBackColor;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            radioButton1.BackColor = DefaultBackColor;
            radioButton2.BackColor = DefaultBackColor;
        }

        private void anuleazaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
