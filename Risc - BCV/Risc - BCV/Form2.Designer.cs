﻿namespace Risc___BCV
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.adaugarePacientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionareFisePacientiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graficInterpretareAnalizePacientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iesireToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.DarkRed;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adaugarePacientToolStripMenuItem,
            this.gestionareFisePacientiToolStripMenuItem,
            this.graficInterpretareAnalizePacientToolStripMenuItem,
            this.iesireToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(513, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // adaugarePacientToolStripMenuItem
            // 
            this.adaugarePacientToolStripMenuItem.Name = "adaugarePacientToolStripMenuItem";
            this.adaugarePacientToolStripMenuItem.Size = new System.Drawing.Size(112, 20);
            this.adaugarePacientToolStripMenuItem.Text = "Adaugare pacient";
            this.adaugarePacientToolStripMenuItem.Click += new System.EventHandler(this.adaugarePacientToolStripMenuItem_Click);
            // 
            // gestionareFisePacientiToolStripMenuItem
            // 
            this.gestionareFisePacientiToolStripMenuItem.Name = "gestionareFisePacientiToolStripMenuItem";
            this.gestionareFisePacientiToolStripMenuItem.Size = new System.Drawing.Size(141, 20);
            this.gestionareFisePacientiToolStripMenuItem.Text = "Gestionare fise pacienti";
            this.gestionareFisePacientiToolStripMenuItem.Click += new System.EventHandler(this.gestionareFisePacientiToolStripMenuItem_Click);
            // 
            // graficInterpretareAnalizePacientToolStripMenuItem
            // 
            this.graficInterpretareAnalizePacientToolStripMenuItem.Name = "graficInterpretareAnalizePacientToolStripMenuItem";
            this.graficInterpretareAnalizePacientToolStripMenuItem.Size = new System.Drawing.Size(195, 20);
            this.graficInterpretareAnalizePacientToolStripMenuItem.Text = "Grafic interpretare analize pacient";
            this.graficInterpretareAnalizePacientToolStripMenuItem.Click += new System.EventHandler(this.graficInterpretareAnalizePacientToolStripMenuItem_Click);
            // 
            // iesireToolStripMenuItem
            // 
            this.iesireToolStripMenuItem.Name = "iesireToolStripMenuItem";
            this.iesireToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.iesireToolStripMenuItem.Text = "Iesire";
            this.iesireToolStripMenuItem.Click += new System.EventHandler(this.iesireToolStripMenuItem_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 275);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.Text = "Calcul risc";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem adaugarePacientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionareFisePacientiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graficInterpretareAnalizePacientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iesireToolStripMenuItem;
    }
}