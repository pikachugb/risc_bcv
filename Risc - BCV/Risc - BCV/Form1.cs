﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Risc___BCV
{
    public partial class Form1 : Form
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=c:\users\puiu moldoveanu\documents\visual studio 2010\Projects\Risc - BCV\Risc - BCV\Riscuri.mdf;Integrated Security=True;User Instance=True");
     
        public Form1()
        {
            InitializeComponent();
            cmd.Connection = cn;
            textBox2.PasswordChar = '*';
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            groupBox2.Visible = false;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            groupBox2.Visible = false;
            if (comboBox1.Text == "Medic")
            {
                groupBox1.Visible = true;
            }
            else
            {
                groupBox2.Visible = true;
            }
        }

        public void sform2()
        {
            Application.Run(new Form2());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "medic" && textBox2.Text == "oti2014")
            {
                MessageBox.Show("Autentificare cu succes! \n :)");
                System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(sform2));
                t.Start();
                this.Close();
            }
            else
            {
                MessageBox.Show("Datele sunt incorecte!");
            }
        }

        private void iesireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender,e);
            }
        }

    }
}
